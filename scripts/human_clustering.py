#!/usr/bin/env python
# -*- coding: utf-8 -*-
import math
from sklearn.cluster import KMeans
import numpy as np
import rospy
from kbys_msgs.msg import PoseWithTwistArrayStamped
from kbys_msgs.msg import PoseWithTwist
from kbys_msgs.msg import PoseTwistWithCovariance
from kbys_msgs.msg import PoseTwistWithCovarianceArrayStamped
from sensor_msgs.msg import LaserScan
from visualization_msgs.msg import MarkerArray
from visualization_msgs.msg import Marker
import tf
import time

class HumanCrowd:
    def __init__(self):
        # ros node object
        self.pub_cluster = rospy.Publisher('human_cluster', PoseTwistWithCovarianceArrayStamped, queue_size=100)
        self.pub_visualised_cluster = rospy.Publisher('visualized_human_cluster', MarkerArray, queue_size=100)
        self.r = rospy.Rate(10)  # 10Hz

        # clustering object
        self.NUM_CLUSTER = 4
        self.WEIGHT_d = 1
        self.WEIGHT_w = 100
        self.pred = KMeans(n_clusters=self.NUM_CLUSTER)
        self.counter = 0

        # ros msg objects
        self.cluster = PoseTwistWithCovarianceArrayStamped()
        self.visualized_clustered_human = MarkerArray()
        self.cluster.states = [0 for x in range(self.NUM_CLUSTER)]

        # ros tf object
        self.tf_listener = tf.TransformListener()
        self.base_name = "base_link"
        self.target_link = "map"

    def get_marker_color(self, cluster_marker, idx):  # ベタ書きは今後改善
        if (self.pred.labels_[idx] == 0):
            cluster_marker.color.a = 1
            cluster_marker.color.r = 1
            cluster_marker.color.g = 1
            cluster_marker.color.b = 0
        elif (self.pred.labels_[idx] == 1):
            cluster_marker.color.a = 1
            cluster_marker.color.r = 1
            cluster_marker.color.g = 0
            cluster_marker.color.b = 0
        elif (self.pred.labels_[idx] == 2):
            cluster_marker.color.a = 1
            cluster_marker.color.r = 0
            cluster_marker.color.g = 1
            cluster_marker.color.b = 1
        elif (self.pred.labels_[idx] == 3):
            cluster_marker.color.a = 1
            cluster_marker.color.r = 0
            cluster_marker.color.g = 1
            cluster_marker.color.b = 0
        elif (self.pred.labels_[idx] == 4):
            cluster_marker.color.a = 1
            cluster_marker.color.r = 1
            cluster_marker.color.g = 1
            cluster_marker.color.b = 1
        elif (self.pred.labels_[idx] == 5):
            cluster_marker.color.a = 1
            cluster_marker.color.r = 1
            cluster_marker.color.g = 0.5
            cluster_marker.color.b = 0.2

    def visualize(self, emulated_human_states):
        self.visualized_clustered_human = [Marker() for x in range(len(emulated_human_states.states))]
        for idx, emulated_human_state in enumerate(emulated_human_states.states):
            cluster_marker = Marker()
            cluster_marker.header = self.cluster.header
            cluster_marker.type = Marker.SPHERE
            cluster_marker.action = Marker.ADD
            cluster_marker.id = idx
            cluster_marker.pose.position = emulated_human_state.position
            self.get_marker_color(cluster_marker, idx)
            cluster_marker.ns = "filtered human"
            cluster_marker.scale.x = 0.3
            cluster_marker.scale.y = 0.3
            cluster_marker.scale.z = 0.3
            # print "label: " +str(self.pred.labels_[1])
            self.visualized_clustered_human[idx] = cluster_marker

    def publish(self):
        # rospy.loginfo("cluster human_posetwist has published!!")
        self.pub_cluster.publish(self.cluster)
        self.pub_visualised_cluster.publish(self.visualized_clustered_human)

    def set_covariance_2_rosmsg(self, covariance_matrix, single_cluster):
        NUM_ROSMSG_DATA = 9
        for row_cov in range(NUM_ROSMSG_DATA):
            for col_cov in range(NUM_ROSMSG_DATA):
                if (row_cov == 0 or row_cov == 1) and (col_cov == 0 or col_cov == 1):
                    single_cluster.covariance[col_cov + row_cov * NUM_ROSMSG_DATA] = covariance_matrix[row_cov, col_cov]
                elif (row_cov == 0 or row_cov == 1) and (col_cov == 3 or col_cov == 4):
                    single_cluster.covariance[col_cov + row_cov * NUM_ROSMSG_DATA] = covariance_matrix[
                        row_cov, col_cov - 1]
                elif (row_cov == 3 or row_cov == 4) and (col_cov == 0 or col_cov == 1):
                    single_cluster.covariance[col_cov + row_cov * NUM_ROSMSG_DATA] = covariance_matrix[
                        row_cov - 1, col_cov]
                elif (row_cov == 3 or row_cov == 4) and (col_cov == 3 or col_cov == 4):
                    single_cluster.covariance[col_cov + row_cov * NUM_ROSMSG_DATA] = covariance_matrix[
                        row_cov - 1, col_cov - 1]
                else:
                    single_cluster.covariance[col_cov + row_cov * NUM_ROSMSG_DATA] = 0

    def transform_worldvec_to_robotvec(self,world_vec):
        tf_matrix = self.get_transform_matrix()
        human_posi_world=np.array([world_vec[0], world_vec[1], 0, 1])
        human_velo_world=np.array([world_vec[2], world_vec[3], 0, 1])
        human_posi_robot=np.dot(tf_matrix, human_posi_world)
        human_velo_robot=np.dot(tf_matrix, human_velo_world)
        return np.array([human_posi_robot[0], human_posi_robot[1], human_velo_robot[0], human_velo_robot[1]])

    def get_covariance(self, feature_vec, cluster_indx, single_cluster):
        cluster_feature_list = np.array([])  # change shape to be able to calculate covariance with numpy
        n_data_in_cluster = 0
        for data_indx in range(len(feature_vec)):
            if (self.pred.labels_[data_indx] == cluster_indx):
                feature_vec_in_robot=self.transform_worldvec_to_robotvec(feature_vec[data_indx])  # get feature vec in robot axis
                cluster_feature_list = np.append(cluster_feature_list, feature_vec_in_robot)
                n_data_in_cluster += 1
        cluster_feature_list = np.reshape(cluster_feature_list, (n_data_in_cluster, len(feature_vec_in_robot)))
        if (len(cluster_feature_list) == 1):  # if number of human belonging one cluster is one
            cluster_feature_list = np.append(cluster_feature_list, cluster_feature_list)
            cluster_feature_list = np.reshape(cluster_feature_list, (2, len(feature_vec[0])))
        print "---"
        print cluster_feature_list
        cluster_covariance = np.cov(cluster_feature_list, rowvar=0, bias=1)
        self.set_covariance_2_rosmsg(cluster_covariance, single_cluster)
        print cluster_covariance

    def clustering(self,emulated_human_posetwist):
        feature_vec = np.array([])
        for human_posetwist_state in emulated_human_posetwist.states:
            feature_list = []

            feature_list.append(self.WEIGHT_d * human_posetwist_state.position.x)
            feature_list.append(self.WEIGHT_d * human_posetwist_state.position.y)
            feature_list.append(self.WEIGHT_w * human_posetwist_state.twist.linear.x)
            feature_list.append(self.WEIGHT_w * human_posetwist_state.twist.linear.y)
            feature_vec = np.append(feature_vec, feature_list)
        feature_vec = np.reshape(feature_vec, [len(emulated_human_posetwist.states), 4])
        y = self.pred.fit(feature_vec)
        for cluster_indx in range(self.NUM_CLUSTER):
            single_cluster = PoseTwistWithCovariance()
            single_cluster.position.x = self.pred.cluster_centers_[cluster_indx, 0]
            single_cluster.position.y = self.pred.cluster_centers_[cluster_indx, 1]
            single_cluster.twist.linear.x = self.pred.cluster_centers_[cluster_indx, 2]
            single_cluster.twist.linear.y = self.pred.cluster_centers_[cluster_indx, 3]
            self.get_covariance(feature_vec, cluster_indx, single_cluster)
            self.cluster.states[cluster_indx] = single_cluster

    def tf_human_pos(self, human_pos_w, human_posetwist_state, tf_matrix):
        human_pos_w[0] = human_posetwist_state.position.x
        human_pos_w[1] = human_posetwist_state.position.y
        return np.dot(tf_matrix, human_pos_w)

    def get_transform_matrix(self):
        now = rospy.Time(0)
        self.tf_listener.waitForTransform(self.base_name, self.target_link, now, rospy.Duration(3.0))
        (trans, rot) = self.tf_listener.lookupTransform(self.base_name, self.target_link, now)
        tf_matrix = self.tf_listener.fromTranslationRotation(trans, rot)
        return tf_matrix

    def callback(self, emulated_human_posetwist):
        rospy.loginfo("topic has been subscribed!!")
        whole_time = time.time()
        print "---------- "
        print "number of clustering sample is " + str(len(emulated_human_posetwist.states))
        self.cluster.header = emulated_human_posetwist.header
        if (len(emulated_human_posetwist.states) > self.NUM_CLUSTER):
            present_time = time.time()
            self.clustering(emulated_human_posetwist)
            print "time taken in clustering is " + str(time.time() - present_time)
        elif (len(emulated_human_posetwist.states) > self.NUM_CLUSTER and self.NUM_CLUSTER * 2 > len(emulated_human_posetwist.states)):
            rospy.logwarn("number of samples is fewr than two times number of clusters")
            self.clustering(emulated_human_posetwist)
        else:
            rospy.logwarn(
                "number of samples is fewer than of clusters...cannot cluster and publish...\n samples: %d, clusters: %d", \
                len(emulated_human_posetwist.states), self.NUM_CLUSTER)
            single_cluster = PoseTwistWithCovariance()
            single_cluster.position.x = 0
            single_cluster.position.x = 0
            single_cluster.position.y = 0
            single_cluster.twist.linear.x = 0
            single_cluster.twist.linear.y = 0
            single_cluster.covariance = [0] * len(single_cluster.covariance)
            single_cluster.covariance = [0 for x in range(len(single_cluster.covariance))]
            self.cluster.states = [single_cluster for i in range(self.NUM_CLUSTER)]
        self.publish()
        self.visualize(emulated_human_posetwist)
        print "time taken in whole process is " + str(time.time() - whole_time)
        print "number of cluster is " + str(len(self.cluster.states))


def listener():
    print "node has started"
    rospy.init_node('clustering', anonymous=True)
    cluster = HumanCrowd()
    human_posetwist = rospy.Subscriber('/emulated_human_velocities', PoseWithTwistArrayStamped, cluster.callback)
    rospy.spin()


if __name__ == "__main__":
    listener()
